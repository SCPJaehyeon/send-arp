#pragma once
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <pcap/pcap.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <ifaddrs.h>
#define ETYPE 0x0608
#define HTYPE 0x0100
#define PTYPE 0x0008
#define HLEN  6
#define PROLEN  4
#define ARP_REQ 0x0100
#define ARP_REP 0x0200
#define MAC_LEN 6
#define IP_LEN  4


struct etherh { //Ethernet header
    u_char dstmac[MAC_LEN];
    u_char srcmac[MAC_LEN];
    uint16_t type; //ARP
};
struct arph { //ARP header
    uint16_t htype; //HTYPE ETHERNET
    uint16_t ptype; //PTYPE IPV4
    uint8_t hlen; //6
    uint8_t prolen; //4
    uint16_t op;
    u_char senmac[MAC_LEN];
    uint8_t senip[IP_LEN];
    u_char tarmac[MAC_LEN];
    uint8_t tarip[IP_LEN];
};
struct packet {
    struct etherh etherh;
    struct arph arph;
};

void usage(char *argv);
bool get_myip(char *dev, uint8_t myip[IP_LEN]);
bool get_mymac(char *dev, u_char mymac[MAC_LEN]);
bool get_senmac(uint32_t SenIP, u_char mymac[MAC_LEN], uint8_t myip[IP_LEN], pcap_t* handle, u_char tarmac[MAC_LEN]);
