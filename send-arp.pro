TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt
LIBS += -lpcap
SOURCES += \
        cpp/main.cpp    \
        cpp/send-arp.cpp

HEADERS += \
        header/send-arp.h
