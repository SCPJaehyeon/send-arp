#include "header/send-arp.h"

void usage(char *argv){
    printf("Usage : %s [Interface] [Sender IP] [Target IP] \n", argv);
    printf("Example) ./send_arp eth0 192.168.0.11 192.168.0.1 \n");
}

bool get_senmac(uint32_t senip, u_char *mymac, uint8_t myip[4], pcap_t* handle, u_char tarmac[6]){
    const u_char broadMac[MAC_LEN] = {0xff,0xff,0xff,0xff,0xff,0xff};
    const u_char nothingMac[MAC_LEN] = {0x00,0x00,0x00,0x00,0x00,0x00};
    struct packet sendpacket = {};

    memcpy(&sendpacket.etherh.dstmac, &broadMac[0], MAC_LEN);
    memcpy(&sendpacket.etherh.srcmac, &mymac[0], MAC_LEN);
    sendpacket.etherh.type = ETYPE;
    sendpacket.arph.htype = HTYPE;
    sendpacket.arph.ptype = PTYPE;
    sendpacket.arph.hlen = HLEN;
    sendpacket.arph.prolen = PROLEN;
    sendpacket.arph.op = ARP_REQ;
    memcpy(&sendpacket.arph.senmac, &mymac[0], MAC_LEN);
    memcpy(&sendpacket.arph.senip, &myip[0], IP_LEN);
    memcpy(&sendpacket.arph.tarmac, &nothingMac[0], MAC_LEN);
    memcpy(&sendpacket.arph.tarip, &senip, IP_LEN);

    int res = pcap_sendpacket(handle,(u_char*)&sendpacket, sizeof(packet)); //send ARP REQUEST Packet
    if(res == -1){
        printf("ARP REQUEST FAIL\n");
    }
    while(true){
        struct pcap_pkthdr *header;
        const u_char *packet_read;
        int res1=pcap_next_ex(handle, &header, &packet_read);
        if(res1==1){
            struct etherh *eth = (struct etherh*)(&packet_read[0]);
            if(eth->type==ETYPE && memcmp(&eth->dstmac, mymac, MAC_LEN)==0){ //check DestinationMAC==MYMAC && ARP Packet
                struct arph *arp = (struct arph*)(&packet_read[sizeof(etherh)]);
                if(arp->op == ARP_REP && memcmp(&arp->senip, &senip, IP_LEN)==0){ //check SenderIP==ARGV[1] && ARP REPLY Packet
                    memcpy(&tarmac[0], &packet_read[6], MAC_LEN);
                    return true;
                }
            }
        }else {
            printf("Target MAC Finding.. \n");
        }
    }
    return false;
}

bool get_myip(char *dev, uint8_t myip[IP_LEN]) //Helped
{
    struct ifaddrs *ifap, *ifa;
    struct sockaddr_in *sa;

    getifaddrs (&ifap);
    for (ifa = ifap; ifa; ifa = ifa->ifa_next) {
        if (strcmp(ifa->ifa_name, dev) == 0)
        {
            if (ifa->ifa_addr->sa_family==AF_INET) {
                sa = (struct sockaddr_in *) ifa->ifa_addr;
                freeifaddrs(ifap);
                memcpy(&myip[0], &sa->sin_addr, IP_LEN);
                return true;
            }
        }
    }
    freeifaddrs(ifap);
    return false;
}

bool get_mymac(char *dev, u_char mymac[MAC_LEN]){ //Helped
    struct ifreq ifr;
    int s;
    if ((s = socket(AF_INET, SOCK_STREAM,0)) < 0) {
        perror("socket");
        return false;
    }
    strcpy(ifr.ifr_name, dev);
    if (ioctl(s, SIOCGIFHWADDR, &ifr) < 0) {
        perror("ioctl");
        return false;
    }
    memcpy(&mymac[0], &ifr.ifr_hwaddr.sa_data[0], MAC_LEN);
    return true;
}
